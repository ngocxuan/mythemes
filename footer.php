<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package ThemeGrill
 * @subpackage ColorNews
 * @since ColorNews 1.0
 */

?>
   <?php
   if( is_active_sidebar( 'colornews_advertisement_above_footer' ) ) {
      if ( !dynamic_sidebar( 'colornews_advertisement_above_footer' ) ):
      endif;
   }
   ?>

   <?php do_action( 'colornews_before_footer' ); ?>
	<footer id="colophon">
      <?php get_sidebar( 'footer' ); ?>
      <div id="bottom-footer">
         <div class="tg-container">
          <div class="tg-inner-wrap">
               <div id="googleMap" style="width:500px;height:380px;"></div>
            </div>
            <div class="tg-inner-wrap">
               <?php colornews_footer_copyright(); ?>
            </div>
           
            
         </div>
      </div>
	</footer><!-- #colophon end -->
   <a href="#masthead" id="scroll-up"><i class="fa fa-arrow-up"></i></a>
</div><!-- #page end -->
<script
src="http://maps.googleapis.com/maps/api/js">
</script>

<script>
var map;
var hochiminh = new google.maps.LatLng(10.762622, 106.660172);

// Add a Home control that returns the user to London
function HomeControl(controlDiv, map) {
  controlDiv.style.padding = '5px';
  var controlUI = document.createElement('div');
  controlUI.style.backgroundColor = 'yellow';
  controlUI.style.border='1px solid';
  controlUI.style.cursor = 'pointer';
  controlUI.style.textAlign = 'center';
  controlUI.title = 'Set map to London';
  controlDiv.appendChild(controlUI);
  var controlText = document.createElement('div');
  controlText.style.fontFamily='Arial,sans-serif';
  controlText.style.fontSize='12px';
  controlText.style.paddingLeft = '4px';
  controlText.style.paddingRight = '4px';
  controlText.innerHTML = '<b>Home<b>'
  controlUI.appendChild(controlText);

  // Setup click-event listener: simply set the map to London
  google.maps.event.addDomListener(controlUI, 'click', function() {
    map.setCenter(hochiminh)
  });
  var marker=new google.maps.Marker({
  position:hochiminh,
  });

marker.setMap(map);
}

function initialize() {
  var mapDiv = document.getElementById('googleMap');
  var myOptions = {
    zoom: 12,
    center: hochiminh,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  map = new google.maps.Map(mapDiv, myOptions);
  // Create a DIV to hold the control and call HomeControl()
  var homeControlDiv = document.createElement('div');
  var homeControl = new HomeControl(homeControlDiv, map);
//  homeControlDiv.index = 1;
  map.controls[google.maps.ControlPosition.TOP_RIGHT].push(homeControlDiv);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<?php wp_footer(); ?>

</body>
</html>