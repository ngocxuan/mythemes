<?php
/**
 * Template to show the front page.
 *
 * @package ThemeGrill
 * @subpackage ColorNews
 * @since ColorNews 1.0
 */
?>

<?php get_header(); ?>

<div id="main" class="clearfix">
   <article class="tg-container">

      <section class="tg-inner-wrap clearfix about-us">
         <h4 class="art-title-1 animation-2 aboutus">VỀ CÔNG TY CHÚNG TÔI</h4>
         <div class="t2-description">
          <h5>CÔNG TY THÁM TỬ VNN</h5>
          <ul class="slogan">
             <li>17 năm kinh nghiệm về lịnh mọi lĩnh vực của thám tử</li>
             <li>Dịch vụ đa dạng đảm bảo uy tín chất lượng</li>
             <li>Chúng tôi xem sự hài lòng của khách hàng là niềm vui của mình</li>
             <li>Phương châm của chúng tôi: <b>Một chữ tín – Vạn niềm tin</b></li>
          </ul>
          <h6 class="thong-tin-cty"></h6>
         </div>
      </section>

      <section class="tg-inner-wrap">
        <h4 class="art-title-1 animation-1">DỊCH VỤ CỦA CHÚNG TÔI</h4>
        <div class="service">
         <?php
          $args = array( 'posts_per_page' => 20, 'offset'=> 0, 'category' => 2 );
          $myposts = get_posts( $args );
          foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
              <div class="view-show">
              <div class="view-hide">
                <a href="<?php the_permalink();?>"><?php the_title(); ?></a>
              </div>
              <?php the_post_thumbnail(); ?> 
              </div>
              
             <?php endforeach; 
             wp_reset_postdata();?>

        </div>
        

      </section><!-- .tg-inner-wrap -->

      


      <section class="tg-inner-wrap clearfix">
         <h4 class="art-title">LÝ DO BẠN NÊN CHỌN CHÚNG TÔI</h4>
         <div class="t2-description">
           <ul class="lydo">
             <li> 17 năm kinh nghiệm hoạt động điều tra trong rất nhiều lĩnh vực từ những sinh hoạt thường ngày đến các hoạt động kinh doanh lớn nhỏ.</li>
             <li> Đội ngũ thám tử được đào tạo bài bản theo tiêu chuẩn quốc tế, hoạt động lấy thông tin một cách chuyên nghiệp, kín đáo và chuẩn xác.</li>
             <li> Hoạt động toàn quốc: chúng tôi có đội ngũ thám tử tại 64 tỉnh thành trên cả nước.</li>
             <li> Danh mục dịch vụ đa dạng: công ty thám tử tư VNN hiện đang cung cấp 19 dịch vụ thám tử khác nhau nhằm đảm bảo phục vụ khách hàng</li>
           </ul>
         </div>
      </section>


       <section class="tg-inner-wrap clearfix msg-khachhang">
         <h4 class="art-title">Khách hàng nói về Thamtu.com</h4>
         <h5 class="slogon">" Sự hài lòng của khách hàng chính là thành công lớn nhất của chúng tôi "</h5>
         <div class="t2-description">
           <article class="bxslider">
               <section class="yk-kh">
                  <img src="<?php echo get_template_directory_uri();?>/img/customer.jpg">
                  <div class="msg">
                     <h6>Bà: Nguyễn Trâm </h6>
                     <p>Bizweb không những hỗ trợ và tư vấn về dịch vụ mà còn đưa ra giải pháp tích cực để giúp nâng cao doanh số bán hàng. Tôi thực sự hài lòng về dịch vụ và thái độ chăm sóc khách hàng của Bizweb.</p>
                  </div>
               </section>
               
               <section class="yk-kh">
                  <img src="<?php echo get_template_directory_uri();?>/img/customer.jpg">
                  <div class="msg">
                     <h6>Bà: Nguyễn Trâm </h6>
                     <p>Bizweb không những hỗ trợ và tư vấn về dịch vụ mà còn đưa ra giải pháp tích cực để giúp nâng cao doanh số bán hàng. Tôi thực sự hài lòng về dịch vụ và thái độ chăm sóc khách hàng của Bizweb.</p>
                  </div>
               </section>


               <section class="yk-kh">
                  <img src="<?php echo get_template_directory_uri();?>/img/customer.jpg">
                  <div class="msg">
                     <h6>Bà: Nguyễn Trâm </h6>
                     <p>Bizweb không những hỗ trợ và tư vấn về dịch vụ mà còn đưa ra giải pháp tích cực để giúp nâng cao doanh số bán hàng. Tôi thực sự hài lòng về dịch vụ và thái độ chăm sóc khách hàng của Bizweb.</p>
                  </div>
               </section>


               <section class="yk-kh">
                  <img src="<?php echo get_template_directory_uri();?>/img/customer.jpg">
                  <div class="msg">
                     <h6>Bà: Nguyễn Trâm </h6>
                     <p>Bizweb không những hỗ trợ và tư vấn về dịch vụ mà còn đưa ra giải pháp tích cực để giúp nâng cao doanh số bán hàng. Tôi thực sự hài lòng về dịch vụ và thái độ chăm sóc khách hàng của Bizweb.</p>
                  </div>
               </section>

            </article>
         </div>
      </section>

       <section class="tg-inner-wrap clearfix">
         <h4 class="art-title">QUY TRÌNH LÀM VIỆC</h4>
         <div class="t2-description quytrinh">
           <div class="step-1">
             <h6>BƯỚC 1</h6>
             <p>Công ty thám tử tư VNN - với phương châm Một chữ tín</p>
           </div>
           <div class="step-2">
             <h6>BƯỚC 2</h6>
             <p>Công ty thám tử tư VNN  lượng dịch vụ uy tín hơn, với </p>
           </div>
           <div class="step-3">
             <h6>BƯỚC 3</h6>
             <p>Công ty thám tử tư VNN </p>
           </div>
           <div class="step-4">
             <h6>BƯỚC 4</h6>
             <p>Công ty thám tử tư VNN </p>
           </div>
         </div>
      </section>


          <section class="tg-inner-wrap clearfix">
         <h4 class="art-title">TIN TỨC THÁM TỬ</h4>
         <div class="tintuc">
         <?php
          $args = array( 'posts_per_page' => 20, 'offset'=> 0, 'category' => 2 );
          $myposts = get_posts( $args );
          foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
              <div class="view-show">
              <div class="view-hide">
                <a href="<?php the_permalink();?>"><?php the_title(); ?></a>
              </div>
              <?php the_post_thumbnail(); ?> 
              </div>
              
             <?php endforeach; 
             wp_reset_postdata();?>

        </div>
        
      </section>
   </article><!-- .tg-container -->
</div><!-- #main -->
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/jquery.bxslider/jquery.bxslider.js"></script>
 <!-- <script type="text/javascript" src="<?php //echo get_template_directory_uri();?>/js/jssor.slider.min.js"></script> -->

<!--<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/header-slider.js"></script>-->
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/home-animation.js"></script>
<?php get_footer(); ?>