var $window = $(window);
var $animation_text = $('.art-title');

function check_in_viewport(){
	var $window_height = $window.height();
	var $window_top = $window.scrollTop();
	var $window_bottom = ($window_height + $window_top);

	$.each($animation_text,function(){
		var $element = $(this);
		var $element_height = $element.outerHeight();
		var $element_top = $element.offset().top;
		var $element_bottom = ($element_height + $element_top);

		if ($element_top <= $window_bottom&& $element_bottom >= $window_top) {
			if (!$element.hasClass('animation-1')) {
				$element.addClass('animation-1');
				$element.next().find("ul li:first-child").animate({
				opacity:1,
				top:"+=40px"
			},//{}
	    // step: function(now,fx) {
     //  $(this).css('transform','translateX(0)');  
   		// }, duration:1000},
			1000).next().animate({
				opacity:1,
				left:"+=100px",
				zoom:"100%"
			},2000).next().animate({
				opacity:1,
				left:"-=300px",
				width:"100%"
			},4000).next().animate({
				opacity:1,
				top:"-=300px",
				width:"100%"
			},6000);
			};
			

			$element.next().find("div[class^='step-']").addClass("step").fadeIn();
			// $element.next().find("ul li:nth-child(2)").animate({
			// 	opacity:1,
			// 	top:"+=80px"
			// },1000);
		}else {
			//$element.removeClass('animation-1');
		}
	})
}

$window.on('scroll resize',check_in_viewport);
$window.trigger("scroll");


$('.bxslider').bxSlider({
    minSlides: 1,
  maxSlides: 2,
  slideWidth: 540,
  slideMargin: 10,
  controls: false,
  moveSlides: 2,
  auto: true,
autoHover: true,
autoControls: true
});

$('.service').bxSlider({
    minSlides: 2,
  maxSlides: 3,
  slideWidth: 370,
  slideMargin: 5,
  controls: true,
  moveSlides: 2,
  auto: true,
  autoHover: true
});

var _this = $('.tintuc').bxSlider({
    minSlides: 2,
  maxSlides: 5,
  slideWidth: 200,
  slideMargin: 10,
  auto: true,
  ticker: true,
  speed: 50000,
  autoHover: true,
  tickerHover: true,
  useCSS: false
});

$('.tintuc').hover(function(){
	_this.stopAuto();
}).mouseleave(function(){
	_this.startAuto();
});
